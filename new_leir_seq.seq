

  /**********************************************************************************
  *
  * ER (LEIR Ring) version (draft) LS2 in MAD X SEQUENCE format
  * Generated the 28-AUG-2020 16:34:47 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

//---------------------- HKICKER        ---------------------------------------------
ER_BPUAE       : HKICKER     , L := 0;     		! Beam Position Pick-up Axial type E
ER_ECDH        : HKICKER     , L := 0;      	! LEIR electron cooler slot, kicker
ER_KQF         : HKICKER     , L := 0;       	! Tune Kicker, LEIR
ER_LMKFH       : HKICKER     , L := 0;     		! Assembly of fast horizontal kicker magnet
ER_MKDFH       : HKICKER     , L := 0;     		! Kicker Magnet - Dipole Fast Horizontal
ER_MKKFH001    : HKICKER     , L := 0;  		! Kicker, Fast Horizontal, LEIR
//---------------------- HMONITOR       ---------------------------------------------
ER_UEH         : HMONITOR    , L := 0;       	! Beam pick-up, horizontal, circular dia 182mm, LEIR
ER_UEH__001    : HMONITOR    , L := 0;  		! Beam pick up, horizontal, rectangular shape for main unit bending magnets, LEIR
ER_UEH__002    : HMONITOR    , L := 0;  		! Beam pick up, horizontal, circular dia 140mm, inside electron cooler, LEIR
//---------------------- KICKER         ---------------------------------------------
ER_DEHV        : KICKER      , L := 0;      	! Corrector magnet, electron cooler, LEIR
ER_DWHV        : KICKER      , L := 0;      	! Corrector in bending arc, LEIR
ER_KCM         : KICKER      , L := 0;       	! Kicker, momentum cooling, LEIR
ER_KDHV        : KICKER      , L := 0;      	! Kicker
ER_MCCARWIP    : KICKER      , L := 0;  		! Corrector magnet, H+V, type DHV/DHN, LEIR
//---------------------- MARKER         ---------------------------------------------
ER_OMK         : MARKER      , L := 0;       	! ER markers
//---------------------- MONITOR        ---------------------------------------------
ER_BIPM        : MONITOR     , L := 0;      	! Beam Ionisation Profile Monitor (AD, LEIR)
ER_BPUAC       : MONITOR     , L := 0;     		! Beam Position Pick-up Axial type C
ER_BSSLH       : MONITOR     , L := 0;     		! Horizontal slit/scraper
ER_BSSLV       : MONITOR     , L := 0;     		! Vertical slit/scraper
ER_BTVLP006    : MONITOR     , L := 0;  		! Beam TV LEIR Type, Pneumatic, variant 006
ER_UCH         : MONITOR     , L := 0;       	! Schottky Pick-up, horizontal, LEIR
ER_UCV         : MONITOR     , L := 0;       	! Schottky Pick-up, vertical, LEIR
ER_UCV__001    : MONITOR     , L := 0;  		! Schottky pick up, vertical, LEIR
ER_UDHV        : MONITOR     , L := 0;     		! Pickup for transverse damper
ER_UWB__001    : MONITOR     , L := 0;  		! Wide band pick up, LEIR
//---------------------- MULTIPOLE      ---------------------------------------------
ER_ECQ         : MULTIPOLE   , L := 0;      	! LEIR electron cooler slot, multipole
ER_MU2HACWP    : MULTIPOLE   , L := 0;  		! Main unit, multipole, LEIR
ER_XFW         : MULTIPOLE   , L := 0;       	! Pole face winding
//---------------------- QUADRUPOLE     ---------------------------------------------
ER_MQNEK002    : QUADRUPOLE  , L := 0.5122;  	! Quadrupole magnet, type QN LEIR
ER_MQNEK003    : QUADRUPOLE  , L := 0.5072;  	! Quadrupole magnet, type QN LEIR
ER_MQNEK004    : QUADRUPOLE  , L := 0.5122;  	! Quadrupole magnet, type QN, LEIR
ER_MQNEK005    : QUADRUPOLE  , L := 0.5172;  	! Quadrupole magnet, type QN, LEIR
ER_MQNEK006    : QUADRUPOLE  , L := 0.1741;  	! Quadrupole magnet, type QN, LEIR.
ER_MQNEK007    : QUADRUPOLE  , L := 0.3431;  	! Quadrupole magnet, type QN, LEIR
ER_MQNEK008    : QUADRUPOLE  , L := 0.2586;  	! Quadrupole magnet, type QN, LEIR
ER_MQNEKFWP    : QUADRUPOLE  , L := 0.5172;  	! Quadrupole magnet, type QN LEIR
ER_MQSBANWP    : QUADRUPOLE  , L := 0.32;  		! Quadrupole magnet, skew, type QSK
//---------------------- RFCAVITY       ---------------------------------------------
ER_CRF         : RFCAVITY    , L := 0.4;
//---------------------- SBEND          ---------------------------------------------
ER_MU2HA002    : SBEND       , L := 0.55842;  	! Artificial point where magnet BAH has been split into two to add the pole face winding elements
ER_MU2HA003    : SBEND       , L := 0.39574;  	! Artificial point. The centre magnet is split up into BI1PM, BI1PO and BI1PI in order to put in monitors. In reality it is only one magnet, part of the main unit multipole BHN
ER_MU2HA004    : SBEND       , L := 0.2620;  	! Artificial point. The centre magnet is split up into BI1PM, BI1PO and BI1PI in order to put in monitors. In reality it is only one magnet, part of the main unit multipole BHN
ER_MU2HA005    : SBEND       , L := 1.4778;  	! Artificial point. The centre magnet is split up into BI1PM, BI1PO and BI1PI in order to put in monitors. In reality it is only one magnet, part of the main unit multipole BHN
ER_MU2HA006    : SBEND       , L := 0.1778;  	! Artificial point. Magnet BI1PI is separated into BI1PII and BI1PIO to give space for an Ionisation Profile Monitor. The IPM is inside BI1PI, which is really only one magnet (together with BI1PM and BI1PO).
ER_MU2HA007    : SBEND       , L := 1.3000;  	! Artificial point. Magnet BI1PI is separated into BI1PII and BI1PIO to give space for an Ionisation Profile Monitor. The IPM is inside BI1PI, which is really only one magnet (together with BI1PM and BI1PO).
//---------------------- SEXTUPOLE      ---------------------------------------------
ER_MXNACIIP    : SEXTUPOLE   , L := 0.33535;  	! Sextupole Magnet, type XN, LEIR
ER_MXSAAIIP    : SEXTUPOLE   , L := 0.33535;  	! Sextupole Magnet, type skew  XLS, LEIR
//---------------------- SOLENOID       ---------------------------------------------
ER_EC0         : SOLENOID    , L := 0.18787;    ! LEIR electron cooler slot, solenoid
ER_EC1         : SOLENOID    , L := 0.48451;    ! LEIR electron cooler slot, solenoid
ER_EC2         : SOLENOID    , L := 0.15578;    ! LEIR electron cooler slot, solenoid
ER_EC3         : SOLENOID    , L := 0.53184;    ! LEIR electron cooler slot, solenoid
ER_EC4         : SOLENOID    , L := 0.110;      ! LEIR electron cooler slot, solenoid
ER_EC5H        : SOLENOID    , L := 1.089;      ! LEIR electron cooler slot, solenoid
ER_ML_BA_WC    : SOLENOID    , L := 0.427;  	! Solenoid magnet, type 13, LEIR
//---------------------- VMONITOR       ---------------------------------------------
ER_UEV         : VMONITOR    , L := 0;       	! Beam pick-up, vertical, circular dia 182mm, LEIR
ER_UEV__001    : VMONITOR    , L := 0;  		! Beam pick up, vertical, rectangular shape for main unit bending magnets, LEIR
ER_UEV__002    : VMONITOR    , L := 0;  		! Beam pick up, vertical, circular dia 140mm, inside electron cooler, LEIR


/************************************************************************************/
/*                       LEIR Ring SEQUENCE                                         */
/************************************************************************************/

/*********************************************************************/
/* ARC 10 															 */
/*********************************************************************/

ER.BHN10 : SEQUENCE, refer = centre, L = 6.68224;
 ER.BA1HO10                    : ER_MU2HA002     , at = 0.27921  , slot_id = 5986738, assembly_id= 5979962;
 ER.DWHV11                     : ER_DWHV         , at = 0.55842  , slot_id = 6029882, assembly_id= 5979962;
 ER.XFW11                      : ER_XFW          , at = 0.55842  , slot_id = 5987585, assembly_id= 5979962;
 ER.BA1HI10                    : ER_MU2HA002     , at = 0.83763  , slot_id = 5986739, assembly_id= 5979962;
 ER.BI1PO10                    : ER_MU2HA003     , at = 1.33046  , slot_id = 5986743, assembly_id= 5979962;
 ER.UEV13                      : ER_UEV__001     , at = 1.52833  , slot_id = 5986366, assembly_id= 5979962;
 ER.BI1PM10                    : ER_MU2HA004     , at = 1.65933  , slot_id = 5986746, assembly_id= 5979962;
 ER.UEH13                      : ER_UEH__001     , at = 1.79033  , slot_id = 5986367, assembly_id= 5979962;
 ER.BI1PI10                    : ER_MU2HA005     , at = 2.52923  , slot_id = 5986749, assembly_id= 5979962;
 !ER.BHN10                      : ER_MU2HACWP     , at = 3.34112  , slot_id = 5979962;
 ER.UCH10                      : ER_UCH          , at = 3.34112  , slot_id = 6029904, assembly_id= 5979962;
 ER.BI2PI10                    : ER_MU2HA005     , at = 4.15301  , slot_id = 5986750, assembly_id= 5979962;
 ER.UEH14                      : ER_UEH__001     , at = 4.89191  , slot_id = 5986368, assembly_id= 5979962;
 ER.BI2PM10                    : ER_MU2HA004     , at = 5.02291  , slot_id = 5986747, assembly_id= 5979962;
 ER.UEV14                      : ER_UEV__001     , at = 5.15391  , slot_id = 5986369, assembly_id= 5979962;
 ER.BI2PO10                    : ER_MU2HA003     , at = 5.35178  , slot_id = 5986744, assembly_id= 5979962;
 ER.BA2HI10                    : ER_MU2HA002     , at = 5.84461  , slot_id = 5986740, assembly_id= 5979962;
 ER.DWHV12                     : ER_DWHV         , at = 6.12382  , slot_id = 6029883, assembly_id= 5979962;
 ER.XFW12                      : ER_XFW          , at = 6.12382  , slot_id = 5987586, assembly_id= 5979962;
 ER.BA2HO10                    : ER_MU2HA002     , at = 6.40303  , slot_id = 5986741, assembly_id= 5979962;
ENDSEQUENCE;

/*********************************************************************/
/* ARC 20 															 */
/*********************************************************************/

ER.BHN20 : SEQUENCE, refer = centre, L = 6.68224;
 ER.BA1HO20                    : ER_MU2HA002     , at = 0.27921    , slot_id = 5986966, assembly_id= 5979964;
 ER.DWHV21                     : ER_DWHV         , at = 0.55842    , slot_id = 6029884, assembly_id= 5979964;
 ER.XFW21                      : ER_XFW          , at = 0.55842    , slot_id = 5987591, assembly_id= 5979964;
 ER.BA1HI20                    : ER_MU2HA002     , at = 0.83763    , slot_id = 5986967, assembly_id= 5979964;
 ER.BI1PO20                    : ER_MU2HA003     , at = 1.33046    , slot_id = 5986970, assembly_id= 5979964;
 ER.UEV23                      : ER_UEV__001     , at = 1.52833    , slot_id = 5986675, assembly_id= 5979964;
 ER.BI1PM20                    : ER_MU2HA004     , at = 1.65933    , slot_id = 5986972, assembly_id= 5979964;
 ER.UEH23                      : ER_UEH__001     , at = 1.79033    , slot_id = 5986683, assembly_id= 5979964;
 ER.BI1PI20                    : ER_MU2HA005     , at = 2.52923    , slot_id = 5986974, assembly_id= 5979964;
 !ER.BHN20                      : ER_MU2HACWP     , at = 3.34112    , slot_id = 5979964;
 ER.BI2PI20                    : ER_MU2HA005     , at = 4.15301    , slot_id = 5986975, assembly_id= 5979964;
 ER.UEH24                      : ER_UEH__001     , at = 4.89191    , slot_id = 5986684, assembly_id= 5979964;
 ER.BI2PM20                    : ER_MU2HA004     , at = 5.02291    , slot_id = 5986973, assembly_id= 5979964;
 ER.UEV24                      : ER_UEV__001     , at = 5.15391    , slot_id = 5986676, assembly_id= 5979964;
 ER.BI2PO20                    : ER_MU2HA003     , at = 5.35178    , slot_id = 5986971, assembly_id= 5979964;
 ER.BA2HI20                    : ER_MU2HA002     , at = 5.84461    , slot_id = 5986968, assembly_id= 5979964;
 ER.DWHV22                     : ER_DWHV         , at = 6.12382    , slot_id = 6029885, assembly_id= 5979964;
 ER.XFW22                      : ER_XFW          , at = 6.12382    , slot_id = 5987592, assembly_id= 5979964;
 ER.BA2HO20                    : ER_MU2HA002     , at = 6.40303    , slot_id = 5986969, assembly_id= 5979964; 
ENDSEQUENCE;

/*********************************************************************/
/* ARC 30 															 */
/*********************************************************************/

ER.BHN30 : SEQUENCE, refer = centre, L = 6.68224;
 ER.BA1HO30                    : ER_MU2HA002     , at = 0.27921    , slot_id = 5987207, assembly_id= 5979965;
 ER.DWHV31                     : ER_DWHV         , at = 0.55842    , slot_id = 6029886, assembly_id= 5979965;
 ER.XFW31                      : ER_XFW          , at = 0.55842    , slot_id = 5987595, assembly_id= 5979965;
 ER.BA1HI30                    : ER_MU2HA002     , at = 0.83763    , slot_id = 5987208, assembly_id= 5979965;
 ER.BI1PO30                    : ER_MU2HA003     , at = 1.33046    , slot_id = 5987241, assembly_id= 5979965;
 ER.UEV33                      : ER_UEV__001     , at = 1.52833    , slot_id = 5986677, assembly_id= 5979965;
 ER.BI1PM30                    : ER_MU2HA004     , at = 1.65933    , slot_id = 5987251, assembly_id= 5979965;
 ER.UEH33                      : ER_UEH__001     , at = 1.79033    , slot_id = 5986685, assembly_id= 5979965;
 ER.BI1PI30                    : ER_MU2HA005     , at = 2.52923    , slot_id = 5987261, assembly_id= 5979965;
 !ER.BHN30                      : ER_MU2HACWP     , at = 3.34112    , slot_id = 5979965;
 ER.BI2PI30                    : ER_MU2HA005     , at = 4.15301    , slot_id = 5987263, assembly_id= 5979965;
 ER.UEH34                      : ER_UEH__001     , at = 4.89191    , slot_id = 5986686, assembly_id= 5979965;
 ER.BI2PM30                    : ER_MU2HA004     , at = 5.02291    , slot_id = 5987253, assembly_id= 5979965;
 ER.UEV34                      : ER_UEV__001     , at = 5.15391    , slot_id = 5986678, assembly_id= 5979965;
 ER.BI2PO30                    : ER_MU2HA003     , at = 5.35178    , slot_id = 5987243, assembly_id= 5979965;
 ER.BA2HI30                    : ER_MU2HA002     , at = 5.84461    , slot_id = 5987209, assembly_id= 5979965;
 ER.DWHV32                     : ER_DWHV         , at = 6.12382    , slot_id = 6029887, assembly_id= 5979965;
 ER.XFW32                      : ER_XFW          , at = 6.12382    , slot_id = 5987596, assembly_id= 5979965;
 ER.BA2HO30                    : ER_MU2HA002     , at = 6.40303    , slot_id = 5987210, assembly_id= 5979965;
ENDSEQUENCE;

/*********************************************************************/
/* ARC 40 															 */
/*********************************************************************/

ER.BHN40 : SEQUENCE, refer = centre, L = 6.68224;
 ER.BA1HO40                    : ER_MU2HA002     , at = 0.27921   , slot_id = 5987215, assembly_id= 5979966;
 ER.DWHV41                     : ER_DWHV         , at = 0.55842   , slot_id = 6029888, assembly_id= 5979966;
 ER.XFW41                      : ER_XFW          , at = 0.55842   , slot_id = 5987599, assembly_id= 5979966;
 ER.BA1HI40                    : ER_MU2HA002     , at = 0.83763   , slot_id = 5987218, assembly_id= 5979966;
 ER.BI1PO40                    : ER_MU2HA003     , at = 1.33046   , slot_id = 5987245, assembly_id= 5979966;
 ER.UEV43                      : ER_UEV__001     , at = 1.52833   , slot_id = 5986681, assembly_id= 5979966;
 ER.BI1PM40                    : ER_MU2HA004     , at = 1.65933   , slot_id = 5987255, assembly_id= 5979966;
 ER.UEH43                      : ER_UEH__001     , at = 1.79033   , slot_id = 5986689, assembly_id= 5979966;
 ER.BI1PIO40                   : ER_MU2HA006     , at = 1.87923   , slot_id = 5987282, assembly_id= 5979966;
 ER.MPIH41                     : ER_BIPM         , at = 1.96813   , slot_id = 6029677, assembly_id= 5979966;
 ER.BI1PII40                   : ER_MU2HA007     , at = 2.61813   , slot_id = 5987286, assembly_id= 5979966;
 !ER.BHN40                      : ER_MU2HACWP     , at = 3.34112   , slot_id = 5979966;
 ER.UCH40                      : ER_UCH          , at = 3.34112   , slot_id = 6029903, assembly_id= 5979966;
 ER.BI2PI40                    : ER_MU2HA005     , at = 4.15301   , slot_id = 5987266, assembly_id= 5979966;
 ER.UEH44                      : ER_UEH__001     , at = 4.89191   , slot_id = 5986690, assembly_id= 5979966;
 ER.BI2PM40                    : ER_MU2HA004     , at = 5.02291   , slot_id = 5987258, assembly_id= 5979966;
 ER.UEV44                      : ER_UEV__001     , at = 5.15391   , slot_id = 5986682, assembly_id= 5979966;
 ER.BI2PO40                    : ER_MU2HA003     , at = 5.35178   , slot_id = 5987248, assembly_id= 5979966;
 ER.BA2HI40                    : ER_MU2HA002     , at = 5.84461   , slot_id = 5987221, assembly_id= 5979966;
 ER.DWHV42                     : ER_DWHV         , at = 6.12382   , slot_id = 6029889, assembly_id= 5979966;
 ER.XFW42                      : ER_XFW          , at = 6.12382   , slot_id = 5987600, assembly_id= 5979966;
 ER.BA2HO40                    : ER_MU2HA002     , at = 6.40303   , slot_id = 5987224, assembly_id= 5979966;
ENDSEQUENCE;

/*********************************************************************/
/* STRAIGHT SECTION 10												 */
/*********************************************************************/

SS10 : SEQUENCE, refer = centre, L = 12.95368567;
 ER.UQF11                      : ER_BPUAC        , at = .36134       , slot_id = 5980302;
 ER.DFH11                      : ER_MKDFH        , at = .70434       , slot_id = 5980059;
 ER.QDN11                      : ER_MQNEK004     , at = 1.309843     , slot_id = 5979754;
 ER.XDN11                      : ER_MXNACIIP     , at = 1.812343     , slot_id = 5979855;
 ER.QFN11                      : ER_MQNEK003     , at = 2.312343     , slot_id = 5979864;
 ER.XFN11                      : ER_MXNACIIP     , at = 2.842343     , slot_id = 5979867;
 ER.UEH11                      : ER_UEH          , at = 3.572843     , slot_id = 5986360, assembly_id= 6030166;
 ER.UEV11                      : ER_UEV          , at = 3.662843     , slot_id = 5986361, assembly_id= 6030166;
 ER.SMH11                      : ER_OMK          , at = 4.623343     , slot_id = 5987644;
 ER.CENTERSS10				   : ER_OMK			 , at = 6.476843;
 ER.SEH10                      : ER_OMK          , at = 6.476843     , slot_id = 5987642;
 ER.DHV12                      : ER_MCCARWIP     , at = 7.222843     , slot_id = 5980261;
 ER.KEM12                      : ER_KCM          , at = 7.582343     , slot_id = 6036754;
 ER.UEH12                      : ER_UEH          , at = 7.846343     , slot_id = 5987578;
 ER.UEV12                      : ER_UEV          , at = 7.846343     , slot_id = 5987581;
 ER.MSIEVE12                   : ER_OMK          , at = 8.036343     , slot_id = 5980263;
 ER.MTV12                      : ER_BTVLP006     , at = 8.266343     , slot_id = 5980262;
 ER.MTR12                      : ER_OMK          , at = 8.879843     , slot_id = 5979938;
 ER.MTRF12                     : ER_OMK          , at = 9.413843     , slot_id = 5979971;
 ER.XFN12                      : ER_MXNACIIP     , at = 10.111343    , slot_id = 5979868;
 ER.QFN12                      : ER_MQNEK003     , at = 10.641343    , slot_id = 5979865;
 ER.XDN12                      : ER_MXNACIIP     , at = 11.141343    , slot_id = 5979869;
 ER.QDN12                      : ER_MQNEK002     , at = 11.643843    , slot_id = 5979866;
 ER.DFH12                      : ER_MKDFH        , at = 12.249343    , slot_id = 5980060;
 ER.KQF12                      : ER_BPUAE        , at = 12.647343    , slot_id = 6029672;
ENDSEQUENCE;

/*********************************************************************/
/* STRAIGHT SECTION 20												 */
/*********************************************************************/

SS20 : SEQUENCE, refer = centre, L = 12.95368567;
 ER.KDHV21                     : ER_KDHV         , at = 0.318343   , slot_id = 6029679;
 ER.QFN21                      : ER_MQNEKFWP     , at = 0.802343   , slot_id = 5979871;
 ER.DFH21                      : ER_MKDFH        , at = 1.262343   , slot_id = 5980061;
 ER.QDN21                      : ER_MQNEK004     , at = 1.719843   , slot_id = 5979872;
 ER.QSK21                      : ER_MQSBANWP     , at = 2.226842   , slot_id = 5979933;
 ER.QFN23                      : ER_MQNEK002     , at = 2.724843   , slot_id = 5979873;
 ER.SOL21                      : ER_ML_BA_WC     , at = 3.352842   , slot_id = 5979968;
 ER.DEHV21                     : ER_DEHV         , at = 3.917843   , slot_id = 6030224;
 ER.EC021                      : ER_EC0          , at = 4.011778   , slot_id = 5986707;
 ER.ECQSI1                     : ER_ECQ          , at = 4.105713   , slot_id = 5986720;
 ER.EC121                      : ER_EC1          , at = 4.347968   , slot_id = 5986709;
 ER.ECDH1                      : ER_ECDH         , at = 4.590223   , slot_id = 5986725;
 ER.EC221                      : ER_EC2          , at = 4.668113   , slot_id = 5986711;
 ER.ECQSI2                     : ER_ECQ          , at = 4.746003   , slot_id = 5986721;
 ER.EC321                      : ER_EC3          , at = 5.011923   , slot_id = 5986713;
 ER.UEV21                      : ER_UEV__002     , at = 5.277843   , slot_id = 5986370;
 ER.EC421                      : ER_EC4          , at = 5.332843   , slot_id = 5986715;
 ER.UEH21                      : ER_UEH__002     , at = 5.387843   , slot_id = 5986371;
 ER.EC5H21                     : ER_EC5H         , at = 5.932343   , slot_id = 5986717;
 ER.CENTERSS20				   : ER_OMK			 , at = 6.476843;
 ER.EC5H22                     : ER_EC5H         , at = 7.021343   , slot_id = 5986718;
 ER.UEH22                      : ER_UEH__002     , at = 7.565843   , slot_id = 5986372;
 ER.EC422                      : ER_EC4          , at = 7.620843   , slot_id = 5986716;
 ER.UEV22                      : ER_UEV__002     , at = 7.675843   , slot_id = 5986373;
 ER.EC322                      : ER_EC3          , at = 7.941763   , slot_id = 5986714;
 ER.ECQS02                     : ER_ECQ          , at = 8.207683   , slot_id = 5986722;
 ER.EC222                      : ER_EC2          , at = 8.285573   , slot_id = 5986712;
 ER.ECDH2                      : ER_ECDH         , at = 8.363463   , slot_id = 5986726;
 ER.EC122                      : ER_EC1          , at = 8.605718   , slot_id = 5986710;
 ER.ECQS01                     : ER_ECQ          , at = 8.847973   , slot_id = 5986723;
 ER.EC022                      : ER_EC0          , at = 8.941908   , slot_id = 5986708;
 ER.DEHV22                     : ER_DEHV         , at = 9.035843   , slot_id = 6030225;
 ER.SOL22                      : ER_ML_BA_WC     , at = 9.600842   , slot_id = 5979969;
 ER.QFN24                      : ER_MQNEK004     , at = 10.228843   , slot_id = 5979874;
 ER.QSK22                      : ER_MQSBANWP     , at = 10.726842   , slot_id = 5979934;
 ER.QDN22                      : ER_MQNEK002     , at = 11.233843   , slot_id = 5979875;
 ER.QFN22I                     : ER_MQNEK006     , at = 11.979793    , slot_id = 5986325, assembly_id= 5986322;
 ER.UCV22                      : ER_UCV          , at = 12.066843    , slot_id = 5980387;
 !ER.QFN22                      : ER_MQNEK005     , at = 12.151343   , slot_id = 5986322;
 ER.QFN22O                     : ER_MQNEK007     , at = 12.238393   , slot_id = 5986329, assembly_id= 5986322;
ENDSEQUENCE;

/*********************************************************************/
/* STRAIGHT SECTION 30												 */
/*********************************************************************/

SS30 : SEQUENCE, refer = centre, L = 12.95368567;
 ER.UWB31                      : ER_UWB__001     , at = 0.439      , slot_id = 6030227;
 ER.KQF31                      : ER_KQF          , at = 0.73       , slot_id = 6030229;
 ER.QDN31                      : ER_MQNEK004     , at = 1.309843   , slot_id = 5979921;
 ER.XDN31                      : ER_MXNACIIP     , at = 1.812343   , slot_id = 5979870;
 ER.QFN31                      : ER_MQNEK003     , at = 2.312343   , slot_id = 5979922;
 ER.XFN31                      : ER_MXNACIIP     , at = 2.842343   , slot_id = 5979929;
 ER.KFH31                      : ER_MKKFH001     , at = 3.480843   , slot_id = 5980086;
 ER.UEH31                      : ER_UEH          , at = 3.903343   , slot_id = 5986362, assembly_id= 6030248;
 ER.UEV31                      : ER_UEV          , at = 3.993343   , slot_id = 5986363, assembly_id= 6030248;
 ER.DHV31                      : ER_MCCARWIP     , at = 4.726843   , slot_id = 5979956;
 ER.CENTERSS30				   : ER_OMK			 , at = 6.476843;
 ER.UEH32                      : ER_UEH          , at = 8.165343   , slot_id = 5986364, assembly_id= 6079970;
 ER.UEV32                      : ER_UEV          , at = 8.255343   , slot_id = 5986365, assembly_id= 6079970;
 ER.KFH3234                    : ER_LMKFH        , at = 9.235343   , slot_id = 5987173;
 ER.XFN32                      : ER_MXNACIIP     , at = 10.111343  , slot_id = 5979930;
 ER.QFN32                      : ER_MQNEK003     , at = 10.641343  , slot_id = 5979923;
 ER.XDN32                      : ER_MXNACIIP     , at = 11.141343  , slot_id = 5979931;
 ER.QDN32                      : ER_MQNEK002     , at = 11.643843  , slot_id = 5979924;
 ER.UCV32                      : ER_UCV__001     , at = 12.435343  , slot_id = 6030239;
ENDSEQUENCE;

/*********************************************************************/
/* STRAIGHT SECTION 40												 */
/*********************************************************************/

SS40 : SEQUENCE, refer = centre, L = 12.95368567;
 ER.KDHV41                     : ER_KDHV         , at = 0.16        , slot_id = 6029680;
 ER.QFN41I                     : ER_MQNEK008     , at = 0.673043    , slot_id = 5986343, assembly_id= 5986344;
 !ER.QFN41                      : ER_MQNEK005     ,at = 0.802343    , slot_id = 5986344;
 ER.UDHV41                     : ER_UDHV         , at = 0.802343    , slot_id = 6030241;
 ER.QFN41O                     : ER_MQNEK008     , at = 0.931643    , slot_id = 5986347, assembly_id= 5986344;
 ER.QDN41                      : ER_MQNEK004     , at = 1.719843    , slot_id = 5979925;
 ER.XFLS41                     : ER_MXSAAIIP     , at = 2.222343    , slot_id = 5979959;
 ER.QFN43                      : ER_MQNEK002     , at = 2.724843    , slot_id = 5979926;
 ER.UEH41                      : ER_UEH          , at = 3.356343    , slot_id = 5986687;
 ER.UEV41                      : ER_UEV          , at = 3.446343    , slot_id = 5986679;
 ER.DHV41                      : ER_MCCARWIP     , at = 3.668343    , slot_id = 5979972;
 ER.CRF41                      : ER_CRF          , at = 4.290343    , slot_id = 5980088;
 ER.CRF43                      : ER_CRF          , at = 5.390343    , slot_id = 5980089;
 ER.CENTERSS40				   : ER_OMK			 , at = 6.476843; 
 ER.DHV42                      : ER_MCCARWIP     , at = 8.219343    , slot_id = 5979957;
 ER.UEV42                      : ER_UEV          , at = 8.441343    , slot_id = 5986680;
 ER.UEH42                      : ER_UEH          , at = 8.531343    , slot_id = 5986688;
 ER.MPIV42                     : ER_BIPM         , at = 8.743343    , slot_id = 6029676;
 !ER.VCT42                      : ER_OMK          ,at = 9.131343    , slot_id = 5987489;
 ER.MSH42                      : ER_BSSLH        , at = 9.556343    , slot_id = 5979979;
 ER.MSV42                      : ER_BSSLV        , at = 9.752343    , slot_id = 5979981;
 ER.QFN44                      : ER_MQNEK004     , at = 10.228843   , slot_id = 5979927;
 ER.XFLS42                     : ER_MXSAAIIP     , at = 10.731343   , slot_id = 5979960;
 ER.QDN42                      : ER_MQNEK002     , at = 11.233843   , slot_id = 5979928;
 ER.DFH42                      : ER_MKDFH        , at = 11.691343   , slot_id = 5980062;
 ER.QFN42I                     : ER_MQNEK008     , at = 12.021968   , slot_id = 5986350, assembly_id= 5986323;
 !ER.QFN42                      : ER_MQNEK005     ,at = 12.151343   , slot_id = 5986323;
 ER.UDHV42                     : ER_UDHV         , at = 12.151343   , slot_id = 6030242;
 ER.QFN42O                     : ER_MQNEK008     , at = 12.280643   , slot_id = 5986351, assembly_id= 5986323;
 ER.KDHV42                     : ER_KDHV         , at = 12.793686   , slot_id = 6029681;
ENDSEQUENCE;

/*********************************************************************
 * LEIR ring
 *********************************************************************/

LEIR : SEQUENCE, refer = entry, L = 78.54370266;
 SS10,  	AT= 0;
 ER.BHN10, 	AT= 12.95368567;
 SS20,  	AT= 19.63592567;
 ER.BHN20, 	AT= 32.58961133;
 SS30,  	AT= 39.27185133;
 ER.BHN30, 	AT= 52.225537;
 SS40,  	AT= 58.907777;
 ER.BHN40, 	AT= 71.86146266;
ENDSEQUENCE;

/************************************************************************************/
/*                       STRENGTH DEFINITIONS                                       */
/************************************************************************************/

E1ERB    =  0.01157;
E2ERB    = -0.01500;
E3ERB    = -0.09557;
kERBAH   = -0.240964/2;
kERBIPO  = -(Pi/4.-0.240964)*(1-(1.4778+0.2620)/2.13554); 
kERBIPM  = -(Pi/4.-0.240964)*0.2620/2.13554;
kERBIPI  = -(Pi/4.-0.240964)*1.4778/2.13554;
kERBIPIO = -(Pi/4.-0.240964)*0.1778/2.13554;
kERBIPII = -(Pi/4.-0.240964)*1.3000/2.13554;

ER.BA1HO10,                 ANGLE := kERBAH  , E1 = E1ERB;  
ER.BA1HI10, 			    ANGLE := kERBAH  , E2 = E2ERB;  
ER.BI1PO10, 			    ANGLE := kERBIPO;
ER.BI1PM10, 			    ANGLE := kERBIPM;  
ER.BI1PI10, 			    ANGLE := kERBIPI , E2 = E3ERB;  
ER.BI2PI10, 			    ANGLE := kERBIPI , E1 = E3ERB;  
ER.BI2PM10, 			    ANGLE := kERBIPM;  
ER.BI2PO10, 			    ANGLE := kERBIPO; 
ER.BA2HI10, 			    ANGLE := kERBAH  , E1 = E2ERB;  
ER.BA2HO10, 			    ANGLE := kERBAH  , E2 = E1ERB;  
ER.BA1HO20, 			    ANGLE := kERBAH  , E1 = E1ERB;   
ER.BA1HI20, 			    ANGLE := kERBAH  , E2 = E2ERB;  
ER.BI1PO20, 			    ANGLE := kERBIPO; 
ER.BI1PM20, 			    ANGLE := kERBIPM;  
ER.BI1PI20, 			    ANGLE := kERBIPI , E2 = E3ERB;  
ER.BI2PI20, 			    ANGLE := kERBIPI , E1 = E3ERB;  
ER.BI2PM20, 			    ANGLE := kERBIPM;  
ER.BI2PO20, 			    ANGLE := kERBIPO; 
ER.BA2HI20, 			    ANGLE := kERBAH  , E1 = E2ERB;  
ER.BA2HO20, 			    ANGLE := kERBAH  , E2 = E1ERB;  
ER.BA1HO30, 			    ANGLE := kERBAH  , E1 = E1ERB;    
ER.BA1HI30, 			    ANGLE := kERBAH  , E2 = E2ERB;  
ER.BI1PO30, 			    ANGLE := kERBIPO; 
ER.BI1PM30, 			    ANGLE := kERBIPM;  
ER.BI1PI30, 			    ANGLE := kERBIPI , E2 = E3ERB;  
ER.BI2PI30, 			    ANGLE := kERBIPI , E1 = E3ERB;  
ER.BI2PM30, 			    ANGLE := kERBIPM;  
ER.BI2PO30, 			    ANGLE := kERBIPO; 
ER.BA2HI30, 			    ANGLE := kERBAH  , E1 = E2ERB;  
ER.BA2HO30, 			    ANGLE := kERBAH  , E2 = E1ERB;  
ER.BA1HO40, 			    ANGLE := kERBAH  , E1 = E1ERB;    
ER.BA1HI40, 			    ANGLE := kERBAH  , E2 = E2ERB;  
ER.BI1PO40, 			    ANGLE := kERBIPO; 
ER.BI1PM40, 			    ANGLE := kERBIPM;  
ER.BI1PIO40,                ANGLE := kERBIPIO;   
ER.BI1PII40,                ANGLE := kERBIPII, E2 = E3ERB;
ER.BI2PI40,                 ANGLE := kERBIPI , E1 = E3ERB;  
ER.BI2PM40,                 ANGLE := kERBIPM;  
ER.BI2PO40,                 ANGLE := kERBIPO; 
ER.BA2HI40,                 ANGLE := kERBAH  , E1 = E2ERB;  
ER.BA2HO40,                 ANGLE := kERBAH  , E2 = E1ERB; 
ER.SOL21,                   KS := MsolC; 
ER.EC021,                   KS := 0.1059*Msol;
ER.EC121,                   KS := 0.549*Msol;
ER.EC221,                   KS := 0.74*Msol;
ER.EC321,                   KS := 0.9*Msol;
ER.EC421,                   KS := 1.02*Msol;
ER.EC5H21,                  KS := Msol;
ER.EC5H22,                  KS := Msol;
ER.EC422,                   KS := 1.02*Msol;
ER.EC322,                   KS := 0.9*Msol;
ER.EC222,                   KS := 0.74*Msol;
ER.EC122,                   KS := 0.549*Msol;
ER.EC022,                   KS := 0.1059*Msol;
ER.SOL22,                   KS := MsolC;
ER.ECQSI1,                  KSL:={0, -(1/59.5808)*Msol*1.138/0.0756};
ER.ECQSI2,                  KSL:={0, -(1/55.8782)*Msol*1.138/0.0756};
ER.ECQS01,                  KSL:={0, (1/59.5808)*Msol*1.138/0.0756};
ER.ECQS02,                  KSL:={0, (1/55.8782)*Msol*1.138/0.0756};
ER.ECDH1,                   KICK :=  0.022919*Msol*1.138/0.0756;
ER.ECDH2,                   KICK := -0.022919*Msol*1.138/0.0756;
ER.DFH11,                   KICK := kERDFH11;
ER.DFH12,                   KICK := kERDFH12;
ER.DFH21,                   KICK := kERDFH21;
ER.DFH42,                   KICK := kERDFH42;
ER.DWHV11,                  HKICK := kERDWH11, VKICK := kERDWV11;
ER.DHV12,                   HKICK := kERDH12,  VKICK := kERDV12;
ER.DWHV12,                  HKICK := kERDWH12, VKICK := kERDWV12;
ER.DWHV21,                  HKICK := kERDWH21, VKICK := kERDWV21;  
ER.DEHV21,                  HKICK := kERDEH21, VKICK := kERDEV21;
ER.DEHV22,                  HKICK := kERDEH22, VKICK := kERDEV22;
ER.DWHV32,                  HKICK := kERDWH32, VKICK := kERDWV32;
ER.DHV41,                   HKICK := kERDH41,  VKICK := kERDV41;
ER.DHV42,                   HKICK := kERDH42,  VKICK := kERDV42;
ER.DWHV41,                  HKICK := kERDWH41, VKICK := kERDWV41;
ER.DWHV42,                  HKICK := kERDWH42, VKICK := kERDWV42;
ER.QDN11,                   K1 := kERQD1030;
ER.QDN12,                   K1 := kERQD1030;
ER.QDN31,                   K1 := kERQD1030;
ER.QDN32,                   K1 := kERQD1030;             
ER.QFN11,                   K1 := kERQF1030;
ER.QFN12,                   K1 := kERQF1030;
ER.QFN31,                   K1 := kERQF1030;
ER.QFN32,                   K1 := kERQF1030;
ER.QFN41I,                  K1 := kERQF2040;
ER.QFN41O,                  K1 := kERQF2040;
ER.QFN42I,                  K1 := kERQF2040;
ER.QFN42O,                  K1 := kERQF2040;
ER.QFN43,                   K1 := kERQF2344;
ER.QFN44,                   K1 := kERQF2344;
ER.QDN41,                   K1 := kERQD2040;
ER.QDN42,                   K1 := kERQD2040;
ER.QFN21,                   K1 := kERQF2040 + dkFT20;
ER.QFN22I,                  K1 := kERQF2040 + dkFT20;
ER.QFN22o,                  K1 := kERQF2040 + dkFT20;
ER.QFN23,                   K1 := kERQF2344 + dkFT23;
ER.QFN24,                   K1 := kERQF2344 + dkFT23;
ER.QDN21,                   K1 := kERQD2040 + dkDT20;
ER.QDN22,                   K1 := kERQD2040 + dkDT20;
ER.QSK21,                   K1S := kERQSK;
ER.QSK22,                   K1S :=-kERQSK;
ER.XDN11,                   K2 := kERXD1030;
ER.XFN11,                   K2 := kERXF1030;
ER.XFN12,                   K2 := kERXF1030;
ER.XDN12,                   K2 := kERXD1030;
ER.XDN31,                   K2 := kERXD1030; 
ER.XFN31,                   K2 := kERXF1030;
ER.XFN32,                   K2 := kERXF1030;
ER.XDN32,                   K2 := kERXD1030;
ER.XFLS41,                  K2 := kERXF40;  
ER.XFLS42,                  K2 := kERXF40; 

return;


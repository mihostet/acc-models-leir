---
template: overrides/main.html
---

<h1> {{ scn['label'] }} -  {{ conf['label'] }} optics</h1>

## Twiss functions
The Twiss functions of this configuration are shown in the interactive plot below. You can zoom in or hover over any curve to obtain more information about the function's value at a specific element. Below the plot, the Twiss table can be downloaded as TFS file or pickled Pandas DataFrame. 

<object width="100%" height="{{conf['plot_height']}}" data="{{conf['plot_html']}}"></object> 

??? "Twiss table - direct download [TFS]({{ conf['madx'][:-4] + 'tfs' }}), [PKL]({{ conf['madx'][:-4] + 'pkl' }})"
{{ conf['twiss_content'] | indent(8, True) }}

## MAD-X example scripts

You can directly download the necessary MAD-X files for this configuration below.

??? "MAD-X example script - [direct download]({{conf['madx']}})"
{{ conf['madx_content'] | indent(8, True) }}

??? "MAD-X beam command - [direct download]({{conf['beam']}})"
{{ conf['beam_content'] | indent(8, True) }}

??? "MAD-X strength file - [direct download]({{conf['str']}})"
{{ conf['str_content'] | indent(8, True)}}

??? "MAD-X sequence file - [direct download](../../../{{leir_seq}})"
{{ leir_seq_content | indent(8, True)}}
<h1> NOMINAL optics</h1>

<h2> Description </h2>

<p> Operational scenario for the lead ion beams with two bunches produced for the LHC physics programme. 
                                 The magnetic elements of the electron cooler cause perturabations to the optics and the orbit, which are corrected with solenoids, skew quadrupoles and orbit correctors.
                                 The strengths of the magnetic elements of the electron cooler and its corrections are constant along the cycle and not ramped according to the magnetic field. 
                                 A global matching routine is used to compute the corrections and keep the working point the nominal one. The magnetic cycle is displayed in the interactive plot below. The constant magnetic field after 3300 ms cycle time is an artefact of the measurement.</p>

<object width="500px" height=330px  data="magnetic_cycle.html"></object> 
<p style="text-align: center;"><b>Figure 1:</b> Magnetic configuration for the NOMINAL cycle.</p>

<h2> Data table </h2>

The following table contains the most important parameters for each configuration. In addition, the optics functions at the profile monitors are shown.
<p>

<table border="0">
  <tr>
    <th id="CELL1" colspan="10" align=center> </th>
    <th id="CELL2" colspan="4" align = center> <b>MPIV42</b></th>
    <th id="CELL2" colspan="4" align = center> <b>MPIH41</b></th>
    </tr>
  <tr>   
    <th> <b>Configuration</b> </th>
    <th align=center> <b>E<sub>kin</sub> [GeV]</b> </th>
    <th align=center> <b>E<sub>tot</sub> [GeV]</b> </th>
    <th align=center> <b>&gamma;<sub>rel</sub></b> </th>
    <th align=center> <b>&beta;<sub>rel</sub></b> </th>
    <th align=center> <b>p [GeV/c]</b> </th>
    <th align=center> <b>Q<sub>x</sub></b> </th>
    <th align=center> <b>Q<sub>y</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>x</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>y</sub></b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    </tr>
  <tr>
    <td> <a href="0_injection/index.html">injection</a></td>
    <td align="center">0.005</td>
    <td align="center">0.943</td>
    <td align="center">1.005</td>
    <td align="center">0.1</td>
    <td align="center">0.089</td>
    <td align="center">1.813</td>
    <td align="center">2.724</td>
    <td align="center">-0.13</td>
    <td align="center">-0.1</td>
    
    <td align="center">67.651</td>
    <td align="center">5.973</td>
    <td align="center">5.774</td>
    <td align="center">-0.57</td>
    
    <td align="center">73.83</td>
    <td align="center">15.741</td>
    <td align="center">1.517</td>
    <td align="center">-1.077</td>
    <tr>
    <td> <a href="1_flat_bottom/index.html">flat bottom </a></td>
    <td align="center">0.005</td>
    <td align="center">0.943</td>
    <td align="center">1.005</td>
    <td align="center">0.1</td>
    <td align="center">0.089</td>
    <td align="center">1.82</td>
    <td align="center">2.72</td>
    <td align="center">-0.0</td>
    <td align="center">-0.0</td>
    
    <td align="center">67.651</td>
    <td align="center">6.046</td>
    <td align="center">5.978</td>
    <td align="center">0.005</td>
    
    <td align="center">73.83</td>
    <td align="center">15.874</td>
    <td align="center">1.462</td>
    <td align="center">-0.402</td>
    <tr>
    <td> <a href="2_flat_top/index.html">flat top </a></td>
    <td align="center">0.073</td>
    <td align="center">1.011</td>
    <td align="center">1.077</td>
    <td align="center">0.371</td>
    <td align="center">0.373</td>
    <td align="center">1.82</td>
    <td align="center">2.72</td>
    <td align="center">-0.0</td>
    <td align="center">-0.0</td>
    
    <td align="center">67.651</td>
    <td align="center">6.027</td>
    <td align="center">6.023</td>
    <td align="center">0.001</td>
    
    <td align="center">73.83</td>
    <td align="center">15.364</td>
    <td align="center">1.497</td>
    <td align="center">-0.417</td>
    <tr>
    <td> <a href="3_extraction/index.html">extraction</a></td>
    <td align="center">0.073</td>
    <td align="center">1.011</td>
    <td align="center">1.077</td>
    <td align="center">0.371</td>
    <td align="center">0.373</td>
    <td align="center">1.82</td>
    <td align="center">2.718</td>
    <td align="center">-0.0</td>
    <td align="center">-0.02</td>
    
    <td align="center">67.651</td>
    <td align="center">6.038</td>
    <td align="center">6.006</td>
    <td align="center">-0.059</td>
    
    <td align="center">73.83</td>
    <td align="center">15.306</td>
    <td align="center">1.494</td>
    <td align="center">-0.502</td>
    </tr>
 
</table>

<p> Symbols:
<ul>
  <li><strong>E<sub>kin</sub></strong>: Kinetic energy of the beam in GeV. </li>
  <li><strong>E<sub>tot</sub></strong>: Total energy of the beam in GeV. </li>
  <li><strong>&gamma;<sub>rel</sub></strong>: Relativistic &gamma; of the beam. </li>
  <li><strong>&beta;<sub>rel</sub></strong>: Relativistic &beta; of the beam. </li>
  <li><strong>p</strong>: Momentum of the beam in GeV/c. </li>
  <li><strong>s</strong>: Longitudinal position of the device in m. </li>
  <li><strong>&beta;<sub>x,y</sub></strong>: &beta;-functions at the specified beam instrumentation. </li>
  <li><strong>D<sub>x</sub></strong>: Horizontal dispersion function at the specified beam instrumentation. </li>
</ul>

</p>